import * as functions from 'firebase-functions';
import { environment } from './config/environment';
import { DataSnapshot } from 'firebase-functions/lib/providers/database';
const admin = require('firebase-admin');
const geolib = require('geolib');

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
admin.initializeApp();

// // Keeps track of the length of the 'likes' child list in a separate property.
// exports.countlikechange = functions.database.ref(environment.firebase_database_BASE + '{key}/history/{history_each}').onWrite(
//     (change) => {
//       const collectionRef = change.after.ref.parent;
//       const countRef = collectionRef.parent.child('history_count');

//       let increment;
//       if (change.after.exists() && !change.before.exists()) {
//         increment = 1;
//       } else if (!change.after.exists() && change.before.exists()) {
//         increment = -1;
//       } else {
//         return null;
//       }

//       // Return the promise from countRef.transaction() so our function
//       // waits for this async event to complete before it exits.
//       return countRef.transaction((current) => {
//         return (current || 0) + increment;
//       }).then(() => {
//         console.log('Counter updated.');
//         return;
//       });
//     });

exports.createJourneySummary = functions.https.onRequest( (req, res) => {
    const dbRef = admin.database().ref(environment.firebase_database_BASE + '-LNRAMAye_nbvqu-7PK-/history');
    const list_of_locations = [];
    dbRef.once('value')
        .then( (snapshot:DataSnapshot) => {
            console.log(snapshot.numChildren());
            // console.log(snapshot.toJSON());
            // res.send(snapshot.numChildren());
            snapshot.forEach( (each_geo_history):boolean => {
                list_of_locations.push(each_geo_history.val());
                return true;
            });
            console.log(JSON.stringify(list_of_locations));
        });
})


/* this is now done on the fe */
// exports.getDisplacementTillDate = functions.database.ref(environment.firebase_database_BASE + '{key}/current_geo').onWrite(
//     (change) => {
//       const collectionRef = change.after.ref.parent;
//       const start_location = collectionRef.child('start_location');
//       const latest_location = collectionRef.child('current_geo');
//       const displacement = collectionRef.child('displacement');
//     //   

//       return start_location.once('value')
//         .then( (start_location_snapshot) => {
//             const start_location_latitude = ( start_location_snapshot.val() && start_location_snapshot.val().latitude )
//             const start_location_longitude = ( start_location_snapshot.val() && start_location_snapshot.val().longitude )

//             return latest_location.once('value')
//                 .then( (latest_location_snapshot) => {
//                     const latest_location_latitude = ( latest_location_snapshot.val() && latest_location_snapshot.val().latitude )
//                     const latest_location_longitude = ( latest_location_snapshot.val() && latest_location_snapshot.val().longitude )
//                     console.log(latest_location_snapshot.val());

//                     const calculatedDisplacement = geolib.getDistance(
//                         {
//                             latitude: start_location_latitude,
//                             longitude: start_location_longitude
//                         },
//                         {
//                             latitude: latest_location_latitude,
//                             longitude: latest_location_longitude
//                         });
//                     console.log( start_location_latitude + ' ' + start_location_longitude + ' | ' + latest_location_latitude + ' ' + latest_location_longitude + ' = ' + calculatedDisplacement);
//                     return displacement.transaction ( (current) => {
//                         return calculatedDisplacement
//                     })
//                     .then( () => {
//                         console.log('Displacement updated.');
//                         return;

//                     });
//                 });
//         });
    
//     });

// If the number of likes gets deleted, recount the number of likes
exports.recountlikes = functions.database.ref(environment.firebase_database_BASE + '{key}/history/{history_each}/history_count').onDelete((snap) => {
  const counterRef = snap.ref;
  const collectionRef = counterRef.parent.child('history_count');

  // Return the promise from counterRef.set() so our function
  // waits for this async event to complete before it exits.
  return collectionRef.once('value')
      .then((historyData) => counterRef.set(historyData.numChildren()));
});